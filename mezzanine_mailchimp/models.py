#! coding: utf-8

import mailchimp

from django.dispatch.dispatcher import receiver
from mezzanine.conf import settings
from mezzanine.forms.signals import form_valid


@receiver(form_valid)
def suscribe_mailchimp(sender, **kwargs):
    entry = kwargs.get('entry')

    if entry is not None:
        fields_email = entry.form.fields.filter(field_type=3).values_list('id', flat=True)

        if fields_email:
            results_emails = entry.fields.filter(field_id__in=fields_email).values_list('value', flat=True)

            if results_emails:
                list_id = settings.MAILCHIMP_LIST_ID
                api_key = settings.MAILCHIMP_API_KEY

                lists_emails = [{'email': {'email': email}} for email in results_emails]
                try:
                    m = mailchimp.Mailchimp(api_key)

                    print(m.lists.batch_subscribe(list_id, lists_emails, double_optin=False))

                except mailchimp.ListAlreadySubscribedError:
                    print('os emails ja estao associados')

                except mailchimp.Error as e:
                    print('Ocorreu um erro! Entre em contato com contato@uailabs.com.br')
                    print(e)
